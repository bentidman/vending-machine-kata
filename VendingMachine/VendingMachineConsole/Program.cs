﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using VendingMachine.Controller;

namespace VendingMachineConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var controller = new VendingMachineController();
            

            while (true)
            {
                Console.WriteLine("Coins should be entered in following format: diameter,weight");
                Console.WriteLine("To select an item type it's name. ex. cola");
                Console.WriteLine();

                Console.WriteLine(controller.WriteDisplay() );
                var input = Console.ReadLine();
                controller.RecordInput(input);
                Console.Clear();
            }
        }
    }
}
