﻿Feature: Select Product
	As a vendor
	I want customers to select products
	So that I can give them an incentive to put money in the machine

Scenario: Confirm Item Price
	Given cola
	Then the price should be $1.00
	Given chips
	Then the price should be $.50
	Given candy 
	Then the price should be $.65

Scenario Outline: Item price should be displayed when customer has not inserted enough money
	Given no coins have been inserted
	When <item> is selected
	Then the display should be "PRICE <price>"

	Examples: 
	| item  | price |
	| cola  | $1.00 |
	| chips | $0.50  |
	| candy | $0.65  |

@SecondCheck 
Scenario: The customer checks the display a second time
	Given no coins have been inserted
	When cola is selected
	Then the display should be "INSERT COINS"
	Given a nickel has been inserted
	When cola is selected
	Then the display should be "$0.05"

@Success
Scenario Outline: The customer successfully completes purchase
	Given $1.00 was inserted
	When <item> is selected
	Then the display should be "THANK YOU"

	Examples: 
	| item  | 
	| cola  | 
	| chips | 
	| candy | 