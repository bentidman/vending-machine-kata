﻿Feature: Make Change
	As a vendor
	I want customers to receive correct change
	So that they will use the vending machine again

Scenario: Coin bank can return coins to user
When a coin is required
Then a quarter can be returned
Then a dime can be returned
Then a nickel can be returned


Scenario Outline: After purchase, return correct change
	Given The customer has inserted coins totaling $1.30
	When they have selected <item>
	Then the current purchase should have <amount> remaining
	Then coins adding up to <amount> are returned to the customer

	Examples:
	| item  | amount |
	| cola  | 0.30   |
	| chips | 0.80   |
	| candy | 0.65   |

