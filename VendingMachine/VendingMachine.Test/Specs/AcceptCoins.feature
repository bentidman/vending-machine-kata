﻿Feature: AcceptCoins
	As a vendor
	I want a vending machine that accepts coins
	So that I can collect money from the customer

@ValidCurrency
Scenario Outline: Insert a valid coin 
	Given no other coins have been inserted
	When A valid <diameter> and <weight>
	Then the result should be a valid coin is recorded
	Then the result should be the coin has a <value>
	Then the <total> should be tracked
	Then the label should be <label>

	Examples: 
	| diameter | weight | value | total | label        |
	| 0.825    | 5.0    | 0.05  | 0.05  | $0.05        |
	| 0.705    | 2.268  | 0.10  | 0.10  | $0.10        |
	| 0.955    | 5.670  | 0.25  | 0.25  | $0.25        |

@InvalidValidCurrency
Scenario Outline: Insert an invalid coin
	Given no other coins have been inserted
	When An invalid <diameter> and <weight>
	Then the result should be an invalid coin is recorded
	Then the <total> should be tracked
	Then the label should be <label>

	Examples: 
	| diameter | weight | total | label        |
	| 0.750    | 2.5    | 0.00  | INVALID COIN |
	| 0.705    | 2.35   | 0.00  | INVALID COIN |

@ValueIsAccumulated
Scenario Outline: Insert two coins and the value should accumulated
	Given no other coins have been inserted
    When two coins are inserted <firstDiameter> and <firstWeight> and <secondDiameter> and <secondWeight>
    Then the <total> should be tracked
    Then the label should be <label>
	Given no other coins have been inserted
    When two coins are inserted <firstDiameter> and <firstWeight> and <secondDiameter> and <secondWeight>
	Then the console should display the <label>

	Examples: 
	| firstDiameter | firstWeight | secondDiameter | secondWeight | total | label        |
	| 0.825         | 5.0         | 0.825          | 5.0          | 0.10  | $0.10        |
	| 0.825         | 5.0         | 0.705          | 2.268        | 0.15  | $0.15        |
	| 0.825         | 5.0         | 0.955          | 5.670        | 0.30  | $0.30        |
	| 0.705         | 2.268       | 0.705          | 2.268        | 0.20  | $0.20        |
	| 0.705         | 2.268       | 0.955          | 5.670        | 0.35  | $0.35        |
	| 0.955         | 5.670       | 0.955          | 5.670        | 0.50  | $0.50        |
	| 0.955         | 5.670       | 0.705          | 2.35         | 0.25  | INVALID COIN |
	| 0.705         | 2.35        | 0.955          | 5.670        | 0.25  | $0.25        |
	| 0.705         | 2.35        | 0.705          | 2.35         | 0.00  | INVALID COIN |



