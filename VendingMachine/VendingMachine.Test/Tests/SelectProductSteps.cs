﻿using System;

using NUnit;
using NUnit.Framework;

using TechTalk.SpecFlow;

using VendingMachine.Model;
using VendingMachine.Controller;

namespace VendingMachine.Test
{
    [Binding]
    public class SelectProductSteps
    {
        public BaseItem CurrentItem { get; set; }
        public VendingMachineController TestController { get; set; } 

        #region confirm item price
        [Given(@"cola")]
        public void GivenCola()
        {
            CurrentItem = new Cola();
        }

        [Given(@"chips")]
        public void GivenChips()
        {
            CurrentItem = new Chips();
        }

        [Given(@"candy")]
        public void GivenCandy()
        {
            CurrentItem = new Candy();
        }

        [Then(@"the price should be \$(.*)")]
        public void ThenThePriceShouldBe(Decimal price)
        {
            Assert.AreEqual(price, CurrentItem.Price);
        }

        #endregion

        #region Item price should be displayed when customer has not inserted enough money
        [Given(@"no coins have been inserted")]
        public void GivenNoCoinsHaveBeenInserted()
        {
            TestController = new VendingMachineController();
        }

        [When(@"(.*) is selected")]
        public void WhenItemIsSelected(string item)
        {
            TestController.RecordInput(item);
        }

        [Then(@"the display should be ""(.*)""")]
        public void ThenTheDisplayShouldBe(string display)
        {
            Assert.AreEqual(display, TestController.WriteDisplay());
        }
        #endregion

        #region second check
        [Given(@"a nickel has been inserted")]
        public void GivenANickelHasBeenInserted()
        {
            TestController = new VendingMachineController();
            TestController.RecordInput(" 0.825,5.0 ");
        }

        [Then(@"the display should be ""(.*)"""), Scope(Tag = "SecondCheck")]
        public void ThenTheDisplayShouldBeInsertCoins(string display)
        {
            TestController.WriteDisplay();
            Assert.AreEqual(display, TestController.WriteDisplay());
        }
        #endregion

        #region success
        [Given(@"\$(.*) was inserted")]
        public void GivenWasInserted(Decimal p0)
        {
            TestController = new VendingMachineController();
            TestController.RecordInput("0.955,5.670 ");
            TestController.RecordInput("0.955,5.670 ");
            TestController.RecordInput("0.955,5.670 ");
            TestController.RecordInput("0.955,5.670 ");
        }

        #endregion
    }
}
