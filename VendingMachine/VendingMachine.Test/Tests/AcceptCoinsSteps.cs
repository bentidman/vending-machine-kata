﻿using System;

using NUnit;
using NUnit.Framework;

using TechTalk.SpecFlow;

using VendingMachine.Model;
using VendingMachine.Controller;

namespace VendingMachine.Test
{
    [Binding]
    public class AcceptCoinsSteps
    {
        private VendingMachineController TestController { get; set; }
        private Coin TestCoin { get; set; }
        private Purchase TestPurchase { get; set; }

        [Given(@"no other coins have been inserted")]
        public void GivenNoOtherCoinsHaveBeenInserted()
        {
            TestController = new VendingMachineController();
            TestPurchase = TestController.CurrentPurchase;
        }

        #region valid currency
        [When(@"A valid (.*) and (.*)")]
        public void WhenAValidDiameterAndWeight(Decimal diameter, Decimal weight)
        {
            TestCoin = new Coin { Weight = weight, Diameter = diameter };
            TestPurchase.InsertCoin(TestCoin);
        }

        [Then(@"the result should be a valid coin is recorded")]
        public void ThenTheResultShouldBeAValidCoinIsRecorded()
        {
            Assert.True(TestCoin.IsValid);
        }

        [Then(@"the result should be the coin has a (.*)")]
        public void ThenTheResultShouldBeTheCoinHasA(Decimal value)
        {
            Assert.AreEqual(value, TestCoin.Value);
        }


        #endregion

        #region invalid currency

        [When(@"An invalid (.*) and (.*)")]
        public void WhenAnInvalidDiameterAndWeight(Decimal diameter, Decimal weight)
        {
            TestCoin = new Coin { Weight = weight, Diameter = diameter };
            TestPurchase.InsertCoin(TestCoin);
        }


        [Then(@"the result should be an invalid coin is recorded")]
        public void ThenTheResultShouldBeAnInvalidCoinIsRecorded()
        {
            Assert.False(TestCoin.IsValid);
        }

        #endregion

        #region  Value Is Accumulated

        [When(@"two coins are inserted (.*) and (.*) and (.*) and (.*)")]
        public void WhenTwoCoinsAreInsertedAndAndAnd(Decimal firstDiameter, Decimal firstWeight, Decimal secondDiameter, Decimal secondWeight)
        {
            TestController.RecordInput(firstDiameter.ToString() + "," + firstWeight.ToString());
            TestController.RecordInput(secondDiameter.ToString() + "," + secondWeight.ToString());
        }

        [Then(@"the console should display the (.*)")]
        public void ThenTheConsoleShouldDisplayThe(string label)
        {
            Assert.AreEqual(label, TestController.WriteDisplay());
        }

        #endregion

        #region shared
        [Then(@"the label should be (.*)")]
        public void ThenTheLabelShouldBe(string label)
        {
            Assert.AreEqual(label, TestPurchase.Display);
        }

        [Then(@"the (.*) should be tracked")]
        public void ThenTheTotalShouldBeTracked(Decimal total)
        {
            Assert.AreEqual(total, TestPurchase.CurrentTotal);
        }
        #endregion

    }
}
