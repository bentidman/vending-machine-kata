﻿using System;
using System.Collections;
using System.Linq;

using NUnit;
using NUnit.Framework;

using TechTalk.SpecFlow;

using VendingMachine.Model;
using VendingMachine.Controller;

namespace VendingMachine.Test.Tests
{
    [Binding]
    public class MakeChangeSteps
    {
        private VendingMachineController TestController { get; set; }
        private Purchase TestPurchase { get; set; }
        private CoinBank bank { get; set; }

        [When(@"a coin is required")]
        public void WhenACoinIsRequired()
        {
            bank = new CoinBank();
        }

        [Then(@"a quarter can be returned")]
        public void ThenAQuarterCanBeReturned()
        {
            var coin = bank.ReturnQuarter();
            Assert.AreEqual(.25m, coin.Value);
        }

        [Then(@"a dime can be returned")]
        public void ThenADimeCanBeReturned()
        {
            var coin = bank.ReturnDime();
            Assert.AreEqual(.10m, coin.Value);
        }

        [Then(@"a nickel can be returned")]
        public void ThenANickelCanBeReturned()
        {
            var coin = bank.ReturnNickel();
            Assert.AreEqual(.05m, coin.Value);
        }

        [Given(@"The customer has inserted coins totaling \$(.*)")]
        public void GivenTheCustomerHasInsertedCoinsTotaling(Decimal p0)
        {
            TestController = new VendingMachineController();
            TestController.RecordInput("0.955,5.670 ");
            TestController.RecordInput("0.955,5.670 ");
            TestController.RecordInput("0.955,5.670 ");
            TestController.RecordInput("0.955,5.670 ");
            TestController.RecordInput("0.955,5.670 ");
            TestController.RecordInput("0.825,5");
        }

        [When(@"they have selected (.*)")]
        public void WhenTheyHaveSelectedItem(string item)
        {
            TestController.RecordInput(item);
        }


        [Then(@"the current purchase should have (.*) remaining")]
        public void ThenTheCurrentPurchaseShouldHaveRemaining(Decimal amount)
        {
            Assert.AreEqual(amount, TestController.CurrentPurchase.CurrentTotal);
        }

        [Then(@"coins adding up to (.*) are returned to the customer")]
        public void ThenCoinsAddingUpToAmountAreReturnedToTheCustomer(Decimal amount)
        {
            TestController.ReturnChange();
            var change = TestController.Change;
            Assert.AreEqual(amount, change.Sum(x => x.Value));
        }
    }
}
