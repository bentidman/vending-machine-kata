﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public class Purchase
    {
        private List<Coin> _insertedCoins { get; set; }
        private string _display;
        private BaseItem _selectedItem;
        
        public Decimal CurrentTotal
        {
            get
            {
                var value = _insertedCoins.Sum(x => x.Value);

                if(_selectedItem != null)
                {
                    return value - _selectedItem.Price;
                }

                return value;
            }
        }

        public string Display 
        {
            get
            {
                var tempDisplay = _display;
                if (_insertedCoins.Count > 0)
                {
                    _display = CurrentTotal.ToString("C");
                }
                else
                {
                    _display = "INSERT COINS";
                }

                return tempDisplay;
            }
            set
            {
                _display = value;
            }
        }

        public Purchase()
        {
            _insertedCoins = new List<Coin>();
            Display = "INSERT COINS";
        }

        public void InsertCoin(Coin newInsertedCoin)
        {
            if(newInsertedCoin.IsValid)
            {
                _insertedCoins.Add(newInsertedCoin);
                Display = CurrentTotal.ToString("C");
            }
            else
            {
                Display = "INVALID COIN";
            }
        }

        public void CompletePurchaseOfItem(BaseItem item)
        {
            if (item.Price > CurrentTotal)
            {
                Display = "PRICE " + item.Price.ToString("C");
            }
            else
            {
                Display = "THANK YOU";
                _selectedItem = item;
            }
        }
    }
}
