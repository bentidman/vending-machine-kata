﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public class Chips: BaseItem
    {
        public Chips()
        {
            this.Price = .50m;
        }
    }
}
