﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public abstract class BaseItem
    {
        public Decimal Price { get; set; }
    }
}
