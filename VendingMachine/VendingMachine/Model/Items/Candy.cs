﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public class Candy: BaseItem
    {
        public Candy()
        {
            Price = .65m;
        }
    }
}
