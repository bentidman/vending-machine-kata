﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public class Cola: BaseItem
    {
        public Cola()
        {
            Price = 1.00m;
        }
    }
}
