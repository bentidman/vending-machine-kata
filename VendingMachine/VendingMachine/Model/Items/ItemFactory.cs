﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public class ItemFactory
    {
        public static BaseItem CreateItemFromInput(string input)
        {
            if (input.ToLower() == "candy")
            {
                return new Candy();
            }
            else if(input.ToLower() == "chips")
            {
                return new Chips();
            }
            else if(input.ToLower() == "cola")
            {
                return new Cola();
            }

            throw (new Exception("Attempt to create unknown item."));
        }
    }
}
