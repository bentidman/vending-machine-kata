﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public class Coin
    {
        public Decimal Weight { get; set; }
        public Decimal Diameter { get; set; }
        public bool IsValid
        {
            get
            {
                //nickel
                if(Weight == 5.0m && Diameter == 0.825m)
                {
                    return true;
                }
                //dime
                else if (Weight == 2.268m && Diameter == 0.705m)
                {
                    return true;
                }
                //quarter
                else if (Weight == 5.670m && Diameter == 0.955m)
                {
                    return true;
                }

                return false;
            }
        }

        public Decimal Value
        {
            get
            {
                //nickel
                if (Weight == 5.0m && Diameter == 0.825m)
                {
                    return .05m;
                }
                //dime
                else if (Weight == 2.268m && Diameter == 0.705m)
                {
                    return .10m;
                }
                //quarter
                else if (Weight == 5.670m && Diameter == 0.955m)
                {
                    return .25m;
                }

                return 0m;
            }
        }

        public Coin()
        {
        }
    }
}
