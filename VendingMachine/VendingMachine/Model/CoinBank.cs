﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public class CoinBank
    {
        public Coin ReturnQuarter()
        {
            return new Coin() { Diameter = 0.955m, Weight = 5.670m };
        }

        public Coin ReturnDime()
        {
            return new Coin() { Diameter = 0.705m, Weight = 2.268m };
        }

        public Coin ReturnNickel()
        {
            return new Coin() { Diameter = 0.825m, Weight = 5.0m };
        }
    }
}
