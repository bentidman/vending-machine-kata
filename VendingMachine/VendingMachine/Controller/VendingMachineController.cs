﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VendingMachine.Model;

namespace VendingMachine.Controller
{
    public class VendingMachineController
    {
        public Purchase CurrentPurchase { get; set; }
        public CoinBank Bank { get; set; }
        public BaseItem PurchasedItem { get; private set; }
        public List<Coin> Change { get; private set; }

        public VendingMachineController()
        {
            CurrentPurchase = new Purchase();
            Bank = new CoinBank();
        }

        public String WriteDisplay()
        {
            return CurrentPurchase.Display;
        }

        public void RecordInput(string input)
        {
            var unknownInput = input.Split(',');

            //make purchase
            if (unknownInput.Length == 1)
            {
                try
                {

                    this.PurchasedItem = ItemFactory.CreateItemFromInput(input);
                    CurrentPurchase.CompletePurchaseOfItem(PurchasedItem);
                    //TODO: dispense item to bin.
                }
                catch(Exception)
                {
                    //TODO: log error for maintenance person
                }

            }
            //record new coin
            else if (unknownInput.Length == 2)
            {
                var insertedCoin = new Coin() { Diameter = (Decimal.Parse(unknownInput[0])), Weight = (decimal.Parse(unknownInput[1])) };
                
                CurrentPurchase.InsertCoin(insertedCoin);

                if(insertedCoin.IsValid == false)
                {
                    //TODO: return coin to coin tray
                }
            }
        }

        public void ReturnChange()
        {
            Change = new List<Coin>();
            while (CurrentPurchase.CurrentTotal - Change.Sum(x => x.Value) >= .25m)
            {
                Change.Add(Bank.ReturnQuarter());
            }
            while (CurrentPurchase.CurrentTotal - Change.Sum(x => x.Value) >= .10m)
            {
                Change.Add(Bank.ReturnDime());
            }
            while (CurrentPurchase.CurrentTotal - Change.Sum(x => x.Value) >= .05m)
            {
                Change.Add(Bank.ReturnNickel());
            }
            
            //TODO: dispense change to coin tray
        }
    }
}
